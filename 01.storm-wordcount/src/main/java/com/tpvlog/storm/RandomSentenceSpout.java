package com.tpvlog.storm;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.util.Map;
import java.util.Random;

/**
 * Spout用于生产数据，这里从数据源不断获取句子
 * <p>
 * 这段代码最终会在Task中执行
 */
public class RandomSentenceSpout extends BaseRichSpout {
    // 用于生产数据
    private SpoutOutputCollector collector;
    private Random random;

    /**
     * 初始化Spout，比如创建线程池等
     *
     * @param conf
     * @param context
     * @param collector open方法初始化时传入
     */
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        this.random = new Random();
    }

    /**
     * 该方法会被无限循环调用
     */
    public void nextTuple() {
        Utils.sleep(5000);
        String[] sentences = new String[]{"the cow jumped over the moon", "an apple a day keeps the doctor away",
                "four score and seven years ago", "snow white and the seven dwarfs", "i am at two with nature"};

        String sentence = sentences[random.nextInt(sentences.length)];
        // 发射数据，values可以看成是一个tuple
        collector.emit(new Values(sentence));
    }

    /**
     * 定义发射出的每个tuple中的每个field的名称
     *
     * @param declarer
     */
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("sentence"));
    }
}
