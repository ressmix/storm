package com.tpvlog.storm;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.apache.storm.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 单词计数拓扑，组合Spout和Bolt形成一个拓扑
 */
public class WordCountTopology {
    private static final Logger LOGGER = LoggerFactory.getLogger(WordCountTopology.class);

    // 各个组件名字的唯一标识
    private final static String SENTENCE_SPOUT_ID = "sentence-spout";
    private final static String SPLIT_SENTENCE_BOLT_ID = "split-bolt";
    private final static String WORD_COUNT_BOLT_ID = "count-bolt";
    private final static String REPORT_BOLT_ID = "report-bolt";

    // 拓扑名称
    private final static String TOPOLOGY_NAME = "word-count-topology";

    public static void main(String[] args) {
        // 构建一个拓扑Builder
        TopologyBuilder topologyBuilder = new TopologyBuilder();

        // 各个组件的实例
        RandomSentenceSpout sentenceSpout = new RandomSentenceSpout();
        SplitSentenceBolt splitSentenceBolt = new SplitSentenceBolt();
        WordCountBolt wordCountBolt = new WordCountBolt();
        ReportBolt reportBolt = new ReportBolt();

        // 配置第一个组件sentenceSpout
        topologyBuilder.setSpout(SENTENCE_SPOUT_ID, sentenceSpout, 2);

        // 配置第二个组件SplitSentenceBolt,上游为randomSentenceSpout,tuple分组方式为随机分组shuffleGrouping
        topologyBuilder.setBolt(SPLIT_SENTENCE_BOLT_ID, splitSentenceBolt).shuffleGrouping(SENTENCE_SPOUT_ID);

        // 配置第三个组件wordCountBolt,上游为splitSentenceBolt,tuple分组方式为fieldsGrouping
        // 同一个单词将进入同一个task中(理解这里非常重要，否则不同单词进入不同wordCountBolt，会导致统计不准)
        topologyBuilder.setBolt(WORD_COUNT_BOLT_ID, wordCountBolt).fieldsGrouping(SPLIT_SENTENCE_BOLT_ID, new Fields("word"));

        // 配置最后一个组件reportBolt,上游为wordCountBolt,tuple分组方式为globalGrouping,即所有的tuple都进入这一个task中
        topologyBuilder.setBolt(REPORT_BOLT_ID, reportBolt).globalGrouping(WORD_COUNT_BOLT_ID);

        Config config = new Config();

        // 说明是在命令行执行，打算提交到storm集群上去
        if (args != null && args.length > 0) {
            config.setNumWorkers(3);
            try {
                StormSubmitter.submitTopology(args[0], config, topologyBuilder.createTopology());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // 说明是在本地运行
            config.setMaxTaskParallelism(20);

            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology(TOPOLOGY_NAME, config, topologyBuilder.createTopology());

            Utils.sleep(60000);
            cluster.shutdown();
        }
    }
}
