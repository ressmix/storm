package com.tpvlog.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 统计结果上报
 */
public class ReportBolt extends BaseRichBolt {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportBolt.class);

    private Map<String, Long> counts = null;

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        counts = new ConcurrentHashMap<String, Long>();
    }

    public void execute(Tuple tuple) {
        String w = tuple.getStringByField("word");
        Long c = tuple.getLongByField("count");
        counts.put(w, c);

        //打印更新后的结果
        LOGGER.info("--------------------------begin-------------------");
        Set<String> words = counts.keySet();
        for (String word : words) {
            LOGGER.info("@report-bolt@: " + word + " ---> " + counts.get(word));
        }
        LOGGER.info("--------------------------end---------------------");
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        //无下游输出,不需要代码
    }
}
